<?php

/**
 * @file
 * Implements new drush command.
 *
 *   "drush Generate-imagestyle" will generate all the derivatives images found
 *   in the current database.
 */

/**
 * Implementation of hook_drush_command().
 */
function generate_imagestyle_drush_command() {
  $items = array();

  $items['generate-imagestyle'] = array(
    'description' => "Generate all imageStyle in the site..",
    'aliases' => array('gis'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
  );

  return $items;
}

/**
 * Drush command callback.
 */
function drush_generate_imagestyle() {
  $result = db_query('SELECT fid, uri FROM {file_managed} WHERE filemime like :mime AND status = :status', array(
    'mime' => 'image/%',
    'status' => FILE_STATUS_PERMANENT
  ));
  foreach ($result as $img_info) {
    foreach (image_styles() as $style) {
      $derivative_uri = image_style_path($style['name'], $img_info->uri);
      if (file_exists($derivative_uri)) {
        drush_log('Image aleady exist, skip it.', 'success');
        continue;
      }
      // Skip existing files.
      $data = (object) array(
        'style' => $style,
        'img_info' => $img_info,
        'derivative_uri' => $derivative_uri
      );
      image_style_create_derivative($data->style, $data->img_info->uri, $data->derivative_uri);
      drush_log('Generated ' . $derivative_uri, 'success');
    }
  }
}
